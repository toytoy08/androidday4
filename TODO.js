let state = []

const reducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODOS':
            return [...state, {
                topic: action.topic,
                completed: false
            }]
        case 'REMOVE_TODOS':
            return state.filter((each, index) => {
                return index !== action.targetIndex
            })
        case 'TOGGLE_TODOS':
            return state.map((each, index) => {
                if (index === action.targetIndex) {
                    return {
                        ...each,
                        complete: !each.completed
                    }
                }
                return each
            })
    }
}

state = reducer(state, {
    type: 'ADD_TODOS',
    topic: 'เอาผ้าไปซัก'
})

console.log(state)
// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     }
// ]



state = reducer(state, {
    type: 'ADD_TODOS',
    topic: 'รดน้ำต้นไม้'
})

console.log(state)

// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
// ]

state = reducer(state, {
    type: 'ADD_TODOS',
    topic: 'ซื้อพิซซ่า'
})

console.log(state)

// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]

state = reducer(state, {
    type: 'TOGGLE_TODOS',
    targetIndex: 1
})

console.log(state)


// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: true
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]

state = reducer(state, {
    type: 'TOGGLE_TODOS',
    targetIndex: 1
})

console.log(state)


// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]

state = reducer(state, {
    type: 'REMOVE_TODOS',
    targetIndex: 1
})

console.log(state)


// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]