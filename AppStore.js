import { createStore, combineReducers } from 'redux'

import TodosReducer from './TodosReducer'
import CompleteReducer from './CompleteReducer'

const reducers = combineReducers({
        todos: TodosReducer,
        completes: CompleteReducer
    }
)

const store = createStore(reducers)
export default store