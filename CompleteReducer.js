export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_COMPLETE':
            return [...state, true]
        case 'REMOVE_COMPLETE':
            return state.map(
                (item, index) => {
                    return index === action.index ? !item : item
                })
        case 'TOGGLE_COMPLETE':
            return state.filter((item, index) => index !== action.index)
        default:
            return state
    }
}